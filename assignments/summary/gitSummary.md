
- The most widely used modern version control system in the world today is Git.
- A version control system (VCS) allows to track the history of a collection of files. It supports creating different versions of this collection.

- A Git repository contains the history of a collection of files starting from a certain directory.

- Commit is a point in time snapshot of a repo.
- Push moves changes branch to branch.
- A branch is a pointer to the head of a group of commits.
- The git merge command takes the independent lines of development created by git branch and integrate them into a single branch.
- The clone downloads an existing git repository to local computer.
- Fork means a copy of the main repository of a project source code to profile.

![](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)