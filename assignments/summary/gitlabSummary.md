
- Gitlab is a service that provides remote access to Git repositories.
- GitLab is a platform for managing Git repositories.
- GitLab is a user friendly web interface layer on top of Git, which increases the speed of working with Git.
- GitLab provides GitLab Community Edition version for users to locate, on which servers their code is present.
- GitLab provides unlimited number of private and public repositories for free.

Some basic commands are:

The version of the Git can be checked by using the command − $ git --version

Check the changes made to your files with the command − $ git status

Set the username by using the command as −  $ git config --global user.name "USERNAME"

Create a new branch with the command − $ git checkout -b branch-name